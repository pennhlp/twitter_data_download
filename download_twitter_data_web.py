"""
Twitter data download script.
Each line in the input file should be a list of strings joined by tab or space.
The first item is user id, whereas the second is tweet id.

Usage: python download_twitter_data_web.py [input_filename] [output_filename]
Sample: python download_twitter_data_web.py sample_ids.txt out.tsv

The script will download all the tweets that are publicly available.
Note that tweets removed by the users will not be retrieved.

Author: Abeed Sarker, Haitao Cai
Email: abeed@dbmi.emory.edu, hcai@pennmedicine.upenn.edu
"""

import csv
import re
import socket
import sys
import urllib
from bs4 import BeautifulSoup


socket.setdefaulttimeout(20)

ITEM_DICT = {}

try:
    FILENAME_IN = sys.argv[1]
    FILENAME_OUT = sys.argv[2]
    F_OUT = open(FILENAME_OUT, 'wb')
    WRITER = csv.writer(F_OUT, delimiter='\t')

    print 'Starting to download tweets, will take some time...'
    for line in open(FILENAME_IN, 'rb'):
        fields = line.split()
        userid, tweetid = fields[:2]
        text = "Not Available"
        if tweetid in ITEM_DICT:
            text = ITEM_DICT[tweetid]
        else:
            try:
                f = urllib.urlopen('http://twitter.com/{}/status/{}'.format(userid, tweetid))
                html = f.read().replace("</html>", "") + "</html>"
                soup = BeautifulSoup(html, "lxml")
                text = soup.find("div",
                                 {
                                     'data-tweet-id': tweetid
                                 }).find('p', 'tweet-text').get_text()
                ITEM_DICT[tweetid] = text
            except Exception:
                continue

        text = re.sub(r'\s', ' ', text)
        fields.insert(2, text.encode('utf-8'))
        WRITER.writerow(fields)
        # print '\t'.join(fields).encode('utf-8')
        sys.stdout.write('{} tweets downloaded\r'.format(len(ITEM_DICT)))
        sys.stdout.flush()

    F_OUT.close()
    print len(ITEM_DICT), 'tweets downloaded in total'
except IndexError:
    print 'Incorrect arguments specified (maybe you didn\'t specify any arguments..'
    print 'Format: python [scriptname] [inputfilename] > [outputfilename]'
