## Twitter Data Download

The script downloads all the tweet texts that are publicly available.

## Requirements

Python 2.7

### Input Format
Each line in the input file should be a list of strings joined by tab or space.
The first item is user id, whereas the second is tweet id.

### Usage
```bash
python download_twitter_data_web.py [input_filename] [output_filename]
```
Sample:
```
python download_twitter_data_web.py sample_ids.txt out.tsv
```

### Contact
Authors:
- Abeed Sarker (abeed@dbmi.emory.edu)
- Haitao Cai (hcai@pennmedicine.upenn.edu)

### Notes
- Tweets removed by the users will not be retrieved. The same for tweets (re)posted by private accounts.
- Downloading data using `download_twitter_data_web.py` will take a long time.

